#! /bin/env node
import readline from 'readline';
import { parse } from './lib/parse';

async function run(input: AsyncIterable<string>) {
  for await (const line of input) {
    try {
      const msg = parse(line);
      console.log(line);
    } catch (e) {
      continue;
    }
  }
}

run(readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false,
}));