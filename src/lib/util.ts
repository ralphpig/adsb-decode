export function buff_map (buff: Buffer, fn: (byte: number) => any) {
  let out = [];
  for (const byte of buff) {
    out.push(fn(byte));
  }
  return out;
}

export function buff_to_bin (buff: Buffer, chunk_size?: number) {
  return chunk_bin(buff_map(
    buff,
    byte => byte.toString(2).padStart(8, '0')
  ).join(''), chunk_size);
}

export function num_to_bin (n: number, bit_length: number, chunk_size?: number) {
  return chunk_bin(
    n
      .toString(2)
      .padStart(bit_length, '0'),
    chunk_size
  )
}

export function chunk_bin (bin: string, chunk_size = 4) {
  return bin
    .split('')
    .map((c, i) => c + ((i + 1) % chunk_size ? '' : ' '))
    .join('');
}
