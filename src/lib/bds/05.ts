import { Message } from '../parse';
import {
  num_to_bin,
} from '../util';

const SURVEILLANCE_STATUS_MAP = [
  'No Condition',
  'Permanent Alert (emergency)',
  'Temporary Alert',
  'SPI Condition',
];

const surv_mask = 0b110;
const ac_mask = 0b11110000;
const t_mask = 0b1000;
const f_mask = 0b100;

//   9: { hpl: '7.5m', rad: '3m' }, 
//   10: { hpl: '25m', rad: '10m' },
//   11: { hpl: '185.2m', rad: '92.6m' },
//   12: { hpl: '370.4m', rad: '185.2m' },
//   13: { hpl: '926m', rad: '463m' },
//   14: { hpl: '1852m', rad: '926m' },
//   15: { hpl: '3704m', rad: '1852m' },
//   16: { hpl: '18520m', rad: '3704m' },
//   17: { hpl: '37040m', rad: '18520m' },
//   18: { hpl: '37040+m', rad: '18520+m' },
// }

export function air_position ({
  tc,
  data
}: Message) {
  if (!(
    tc === 0 ||
    (tc >= 9 && tc <= 18) ||
    tc === 19 ||
    (tc >= 20 && tc <= 22)
  )) {
    throw new Error(`Type Code (tc) '${tc}' does not have airborne position.`);
  }
  
  const head = data[0]
  const surv_status = (head & 0b110) << 1;
  const single_ant = head & 0b1;
  const buff = data.slice(1);

  // icao 9871: pg 54
  const [ac_high, ac_low] = buff.slice(0, 2);
  const ac = (ac_high << 4) | (ac_low >> 4); // 3.1.2.6.5.4
  const t = (ac_low & 0b1000) >> 3;
  const f = (ac_low & 0b0100) >> 2;

  // Altitude
  let altitude: number | null = null;
  if (tc >= 9 && tc <= 18) {
    const q = (ac & 0b10000) >> 4;
    const N = (
      (ac & 0b111111100000 >> 1) |
       ac & 0b000000001111
    );
    
    if (q) altitude = 25 * N - 1000;
    // else altitude = 1000 * N - 1000;
  }

  return {
    surveillance_status: SURVEILLANCE_STATUS_MAP[surv_status],
    single_antenna: Boolean(single_ant),
    time: t,
    format: f,
    altitude,
  }
}