import { Message } from '../parse';

export interface IdMessage {
  id: string;
  category: string;
};

const ID_CHAR_MAP = [
  '',
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
  '', '', '', '', '',
  ' ',
  '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  '', '', '', '', '', ''
];

type CategoryCode = 1 | 2 | 3 | 4;
const CATEGORY_MAP: Record<CategoryCode, Array<string>> = {
  // Set A
  4: [
    'None',
    'Light (15 Mg)',
    'Medium (34 Mg)',
    'Medium (136 Mg)',
    'High Vortex',
    'Heavy (136+ Mg)',
    'High Speed (5+ g acc, 400+ kt)',
    'Rotorcraft'
  ],
  // Set B
  3: [
    'None',
    'Glider / Sailplane',
    'Lighter-than-air',
    'Parachutist / Skydiver',
    'Ultralight / Hang-glider / Paraglider',
    'Reserved',
    'Unmanned Aerial Vehicle',
    'Space / Transatmospheric Vehicle'
  ],
  // Set C
  2: [
    'None',
    'Surface Vehicle (emergency)',
    'Surface Vehicle (service)',
    'Fixed ground or tethered obstruction',
    'Reserved', 'Reserved', 'Reserved', 'Reserved',
  ],
  // Set D
  1: ['Reserved', 'Reserved', 'Reserved', 'Reserved', 'Reserved', 'Reserved', 'Reserved', 'Reserved']
};

export function id ({
  tc,
  data
}: Message): IdMessage {
  if (!(tc >= 1 && tc <= 4)) {
    throw new Error(`Type Code (tc) '${tc}' does not have identification info.`);
  }

  const cat = data[0] & 0b111;
  const id_buff = data.slice(1);

  let id_bin = BigInt(id_buff.readUIntBE(0, id_buff.length));

  // Split 6 byte id_buff into 8, 6 bit chars;
  const char_set = [];
  for (let i = 0; i < 8; ++i) {
    const char = Number(id_bin & 0b111111n);
    char_set.push(ID_CHAR_MAP[char]);
    id_bin = id_bin >> 6n;
  }
  char_set.reverse();

  return {
    id: char_set.join(''),
    category: CATEGORY_MAP[tc as CategoryCode][cat],
  };
}