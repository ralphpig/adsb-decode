const CRC_GEN = Buffer.from([
  0b11111111,
  0b11111010,
  0b00000100,
  0b10000000,
]);

const five_u_mask = 0b11111000;
const three_l_mask = 0b111;

export interface Message {
  df: number;
  ca: number;
  icao: Buffer;
  tc: number;
  data: Buffer;
  pi: number;
}

// Payload parsing functions
export function df (buff: Buffer) {
  return (buff[0] & five_u_mask) >> 3;
}
export function ca (buff: Buffer) {
  return buff[0] & three_l_mask;
}
export function icao (buff: Buffer) {
  return buff.slice(1, 4);
}
export function tc (buff: Buffer) {
  return (buff[4] & five_u_mask) >> 3;
}
export function data (buff: Buffer) {
  return buff.slice(4, 11);
}
export function pi (buff: Buffer) {
  return buff.slice(11, 14).readUIntBE(0, 3);
}

// Use CRC parity checking
// for i from 0 to N - 24  ==>  msg[i:i + 24] ^= CRC_GEN
// return msg[-24]
export function validate (buff: Buffer) {
  const msg = Buffer.from(buff);

  for (let byte_i = 0; byte_i < msg.length - 3; ++byte_i) {
    for (let bit_i = 0; bit_i < 8; ++bit_i) {
      const bit_mask = 0x80 >> bit_i;
      const bit = msg[byte_i] & bit_mask;

      // ^= CNC_GEN but the bitwise operations are split across 3 byte
      if (bit) {
        msg.writeUInt8(msg[byte_i] ^ (CRC_GEN[0] >> bit_i), byte_i);
        msg.writeUInt8(msg[byte_i + 1] ^ (
          0xFF & ((CRC_GEN[0] << 8 - bit_i) | (CRC_GEN[1] >> bit_i))
        ), byte_i + 1);
        msg.writeUInt8(msg[byte_i + 2] ^ (
          0xFF & ((CRC_GEN[1] << 8 - bit_i) | (CRC_GEN[2] >> bit_i))
        ), byte_i + 2);
        msg.writeUInt8(msg[byte_i + 3] ^ (
          0xFF & ((CRC_GEN[2] << 8 - bit_i) | (CRC_GEN[3] >> bit_i))
        ), byte_i + 3);
      }
    }
  }

  return pi(msg) === 0;
}

export function parse (payload: string): Message {
  const match = payload.match(/\*(.*);/);
  if (match) payload = match[1];

  const buff = Buffer.from(payload, 'hex');
  if (!validate(buff)) throw new Error('Invalid payload')

  return {
    df: df(buff),
    ca: ca(buff),
    icao: icao(buff),
    tc: tc(buff),
    data: data(buff),
    pi: pi(buff),
  };
}

