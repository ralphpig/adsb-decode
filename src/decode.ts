#! /bin/env node

// Type Code	Content
// 1 - 4	Aircraft identification
// 5 - 8	Surface position
// 9 - 18	Airborne position (w/ Baro Altitude)
// 19	Airborne velocities
// 20 - 22	Airborne position (w/ GNSS Height)
// 23 - 27	Reserved
// 28	Aircraft status
// 29	Target state and status information
// 31	Aircraft operation status

import readline from 'readline';

import { Message, parse } from './lib/parse';
import { air_position } from './lib/bds/05';
import { id } from './lib/bds/08';

export function decode (msg: Message) {
  const {
    df,
    ca,
    icao,
    tc,
    data,
    pi,
  } = msg;
  if (df !== 17) throw new Error(`Downlink format (df) '${df}' not supported`);

  // Identification
  if (tc >= 1 && tc <= 4) {
    return id(msg);
  }

  // Surface Position
  if (tc >= 5 && tc <= 8) {
    // HPL (Horizontal protection limit)
    // RAD (Containment radius)
    // {
    //   5: { hpl: '7.5m', rad: '3m' }, 
    //   6: { hpl: '25m', rad: '10m' },
    //   7: { hpl: '185.2m', rad: '92.6m' },
    //   8: { hpl: '185.2+m', rad: '92.6+m' },
    // }
    // out = { ...out, ...id(msg) }
  }

  // Airborne Position
  if (tc >= 9 && tc <= 18) {
    // HPL (Horizontal protection limit)
    // RAD (Containment radius)
    // Barometric altitude
    // Format (F)
    // F = 0: even (time is nearest even 200ms step)
    // F = 1: odd (time is nearest odd 200ms step)
    // Time sync (T = bit 21)
    // T = 0: Not synced to UTC
    // T = 1: Synced to UTC
    // {
    //   9: { hpl: '7.5m', rad: '3m' }, 
    //   10: { hpl: '25m', rad: '10m' },
    //   11: { hpl: '185.2m', rad: '92.6m' },
    //   12: { hpl: '370.4m', rad: '185.2m' },
    //   13: { hpl: '926m', rad: '463m' },
    //   14: { hpl: '1852m', rad: '926m' },
    //   15: { hpl: '3704m', rad: '1852m' },
    //   16: { hpl: '18520m', rad: '3704m' },
    //   17: { hpl: '37040m', rad: '18520m' },
    //   18: { hpl: '37040+m', rad: '18520+m' },
    // }
    return air_position(msg);
  }

  if (tc === 19) {
    // Airborne velocity
    // Altitude: Difference between Barometric and GNSS
  }

  // Airborne Position
  if (tc >= 20 && tc <= 22) {
    // HPL (Horizontal protection limit)
    // RAD (Containment radius)
    // GNSS height altitude
    // 20 - 21: Time sync (T = bit 21)
    // {
    //   20: { hpl: '7.5m', rad: '3m' }, 
    //   21: { hpl: '25m', rad: '10m' },
    //   22: { hpl: '25+m', rad: '10+m' },
    // }
    return air_position(msg);
  }

  // tc: 23 | test purposes
  // tc: 24 | surface system status
  // tc: 25 - 27 | reserved
  // tc: 28 | emergency priority status
  // tc: 29 - 30 | reserved
  
  if (tc === 31) {
    // Aircraft operational status
  }
}

// Runner
async function run(input: AsyncIterable<string>) {
  for await (const line of input) {
    try {
      const msg = parse(line);
      const data = decode(msg);
      if(data) console.log(data);
      // console.log(msg);
      // console.log(decode(msg));
    } catch (e) {
      // TODO: print if not "Invalid input"
      console.warn(e);
      continue;
    }
  }
}

run(readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false,
}));
